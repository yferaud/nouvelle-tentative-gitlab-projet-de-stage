const These=require("../models/these")

exports.createTheses = (req, res, next) => {
    const these = new These({
      title: req.body.title,
      description: req.body.description,
    });
  
    these.save().then(
      () => {
        res.status(201).json({
          message: 'Post saved successfully!'
        });
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
}



exports.modifyTheses=(req, res, next) => {
    const these = new These({
      _id: req.params.id,
      title: req.body.title,
      description: req.body.description,
    });
    These.updateOne({_id: req.params.id}, these).then(
      () => {
        res.status(201).json({
          message: 'These updated successfully!'
        });
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
}


exports.deleteThese=(req, res, next) => {
    These.deleteOne({_id: req.params.id}).then(
      () => {
        res.status(200).json({
          message: 'Deleted!'
        });
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
  }


exports.getOneThese=(req, res, next) => {
    These.findOne({
      _id: req.params.id
    }).then(
      (these) => {
        res.status(200).json(these);
      }
    ).catch(
      (error) => {
        res.status(404).json({
          error: error
        });
      }
    );
}


exports.getAllThese= (req, res, next) => {
    These.find().then(
      (theses) => {
        res.status(200).json(theses);
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
  }