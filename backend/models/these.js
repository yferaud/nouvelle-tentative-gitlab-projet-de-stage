const mongoose = require('mongoose');

const theseSchema = mongoose.Schema({
  title:      { type: String, required: true },
  description:{ type: String, required: true },
  id:         { type: Number, required: true },
});

module.exports = mongoose.model('These', theseSchema);