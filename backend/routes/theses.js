const express = require('express');
const router = express.Router();
/*
const auth= require("../middleware/auth")
*/
const theseController=require('../controllers/theses');


router.post  ('/'   ,   /*auth,*/theseController.createTheses );
router.put   ('/:id',   /*auth,*/theseController.modifyTheses);
router.delete('/:id',   /*auth,*/theseController.deleteThese);
router.get   ('/:id',   /*auth,*/theseController.getOneThese);
router.get   ('/'   ,   /*auth,*/theseController.getAllThese);


module.exports = router;