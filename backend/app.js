const express = require('express');
const app = express();
const userRoutes = require('./routes/user');
const thesesRoutes = require('./routes/theses');
const mongoose = require('mongoose');
mongoose.connect('mongodb+srv://admin:admin@cluster0.ergnh.mongodb.net/theses?retryWrites=true&w=majority',
  { useNewUrlParser: true,
    useUnifiedTopology: true })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));


app.use(express.urlencoded({extended: true}));
app.use(express.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
}); 


app.use('/api/theses', thesesRoutes ); /*
app.use('/api/auth'  , userRoutes);*/


module.exports = app;